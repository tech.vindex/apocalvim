-- Copying and distribution of this file, with or without modification,
-- are permitted in any medium without royalty provided the copyright
-- notice and this notice are preserved.
-- This file is offered as-is, without any warranty.

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Lazy Package Manager and All Plugins
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
  -- autocompletion
  {'hrsh7th/nvim-cmp'},
  {'hrsh7th/cmp-nvim-lsp'},
  {'hrsh7th/cmp-buffer'},
  {'saadparwaiz1/cmp_luasnip'},
  {'hrsh7th/cmp-path'},
  {'L3MON4D3/LuaSnip'},

  -- file manager
  {
    'nvim-neo-tree/neo-tree.nvim',
    branch = 'v2.x',
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-tree/nvim-web-devicons',
      'MunifTanjim/nui.nvim',
      's1n7ax/nvim-window-picker'
    }
  },

  -- tabs
  {'akinsho/bufferline.nvim'},

  -- lsp
  {'neovim/nvim-lspconfig'},
  {'williamboman/nvim-lsp-installer'},
  {'williamboman/mason.nvim', build = ':MasonUpdate'},

  -- color scheme
  {'joshdick/onedark.vim'},
  {'EdenEast/nightfox.nvim'},
  {'whatyouhide/vim-gotham'},
  {'cocopon/iceberg.vim'},
  {'alexanderbluhm/black.nvim'},
  {'Julpikar/night-owl.nvim'},
  {'neg-serg/neg.nvim'},
  {'bluz71/vim-moonfly-colors'},

  -- searching
  {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.4',
    dependencies = {'nvim-lua/plenary.nvim'}
  },

  -- comments
  {'tpope/vim-commentary'},

  -- terminal
  {'akinsho/toggleterm.nvim', version = "*", config = true},
  
  -- dashboard with keys
  {
    'folke/which-key.nvim',
    event = 'VeryLazy',
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 0
    end,
  },

  -- tagbar
  {'preservim/tagbar'},

  -- russian keys
  {'powerman/vim-plugin-ruscmd'},

  -- smart splits
  {'mrjones2014/smart-splits.nvim'},

  -- transparent
  {'xiyaowong/transparent.nvim'},
})


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Base Settings
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- for correct work of nvim-tree
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- editor view
vim.opt.number = true
vim.opt.termguicolors = true
vim.cmd.colorscheme('moonfly')
-- vim.api.nvim_set_hl(0, "NormalFloat", {bg = '#0E0A23'})
-- vim.api.nvim_set_hl(0, "Normal", {bg = ''})
vim.opt.colorcolumn = '81'
vim.opt.scrolloff = 8
vim.cmd [[autocmd FileType text,markdown setlocal cc=0]]
vim.cmd [[autocmd FileType python setlocal cc=80]]
vim.opt.cursorline = true

-- highlight copied text
vim.api.nvim_exec([[
augroup YankHighlight
autocmd!
autocmd TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=300}
augroup end
]], false)

-- common behaviour
vim.opt.hlsearch = true
vim.opt.textwidth = 0
vim.opt.wrap = true
vim.opt.clipboard = 'unnamedplus'
vim.opt.swapfile = false
vim.opt.showcmd = true
vim.opt.mouse = 'a'
vim.opt.splitright = true
vim.opt.splitbelow = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.autoindent = true
vim.opt.smartindent = true
-- two spaces for selected filetypes
vim.cmd [[ autocmd FileType html,css,lua setlocal shiftwidth=2 tabstop=2 ]]
vim.cmd [[ autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 ]]

-- common keymaps

vim.g.mapleader = ' '

vim.keymap.set('v', 'S-Y', '"+y', {})

vim.keymap.set('n', '<leader>c', ':bp<bar>sp<bar>bn<bar>bd<CR>')
vim.keymap.set('n', '<leader>q', ':bufdo bd<CR>:q<CR>')
vim.keymap.set('n', '<leader>w', ':w<CR>')

vim.keymap.set('n', '<C-h>', '<C-w>h<CR>')
vim.keymap.set('n', '<C-l>', '<C-w>l<CR>')
vim.keymap.set('n', '<C-j>', '<C-w>j<CR>')
vim.keymap.set('n', '<C-k>', '<C-w>k<CR>')

vim.keymap.set('n', '|', ':vsplit<CR>')

vim.keymap.set('n', '-', '<C-w>-')
vim.keymap.set('n', '+', '<C-w>+')

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Settings Of Plugins
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- nvim-cmp supports additional completion capabilities
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)
vim.o.completeopt = 'menuone,noselect'
-- luasnip setup
local luasnip = require 'luasnip'

--------------
-- nvim-cmp --
--------------

local cmp = require 'cmp'
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  window = {
     completion = cmp.config.window.bordered(),
     documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = false }),
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end, {'i', 's'}),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end, {'i', 's'})
  }),
  sources = {
    {name = 'nvim_lsp', keyword_length = 2},
    {name = 'nvim_lsp_signature_help'},
    {name = 'luasnip'},
    {name = 'path'},
    {
      name = 'buffer',
      keyword_length = 2,
      option = {
        get_bufnrs = function()
          return vim.api.nvim_list_bufs()
        end
      },
    },
  },
}

---------
-- LSP --
---------

require'mason'.setup{}

local lspconfig = require('lspconfig')
lspconfig.pyright.setup{}
lspconfig.serve_d.setup{}

vim.keymap.set('n', '<leader>lD', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<leader>ld', vim.diagnostic.setloclist)

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd('LspAttach', {
  group = vim.api.nvim_create_augroup('UserLspConfig', {}),
  callback = function(ev)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- Buffer local mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local opts = { buffer = ev.buf }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<leader>lwa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<leader>lwr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<leader>lwl', function()
      print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<leader>lr', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<leader>lc', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<leader>f', function()
      vim.lsp.buf.format { async = true }
    end, opts)
  end,
})

---------------
-- telescope --
---------------

local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fw', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
vim.keymap.set('n', '<leader>fs', builtin.lsp_document_symbols, {})

--------------
-- comments --
--------------

vim.cmd [[ autocmd FileType d setlocal commentstring=//%s ]]

----------------
-- toggleterm --
----------------

require('toggleterm').setup {open_mapping = [[<C-\>]]}
function _G.set_terminal_keymaps()
    local opts = {buffer = 0}
    vim.keymap.set('t', '<esc>', [[<C-\><C-n>]], opts)
    vim.keymap.set('t', 'jk', [[<C-\><C-n>]], opts)
    vim.keymap.set('t', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
    vim.keymap.set('t', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
    vim.keymap.set('t', '<C-k>', [[<Cmd>wincmd k<CR>]], opts)
    vim.keymap.set('t', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
    vim.keymap.set('t', '<C-w>', [[<C-\><C-n><C-w>]], opts)
end
vim.cmd('autocmd! TermOpen term://* lua set_terminal_keymaps()')
vim.keymap.set('n', '<leader>t', ':ToggleTerm direction=horizontal<CR>')

--------------
-- neo-tree --
--------------

require'window-picker'.setup({
    autoselect_one = false,
    include_current = false,
    filter_rules = {
        bo = {
            filetype = {'neo-tree', 'neo-tree-popup', 'notify'},
            buftype = {'terminal', 'quickfix'}
        }
    },
    other_win_hl_color = '#e35e4f'
})
vim.cmd([[ let g:neo_tree_remove_legacy_commands = 1 ]])
require("neo-tree").setup({
  enable_git_status = false,
  window = {
    mappings = {
      ["<space>"] = "none",  -- space conflicts with <leader>
    }
  }
})
vim.keymap.set('n', '<leader>e', ':NeoTreeRevealToggle<CR>')
vim.keymap.set('n', '<leader>v', ':NeoTreeFloatToggle<CR>')

----------------
-- bufferline --
----------------

require("bufferline").setup{
  options = {
    mode = 'buffers',
    offsets = {
      {
        filetype = 'neo-tree',
        padding = 1
      }
    },
    diagnostics = 'nvim-lsp',
    separator_style = 'slope',
  }
}
vim.keymap.set('n', '<S-h>', ':BufferLineCyclePrev<CR>')
vim.keymap.set('n', '<S-l>', ':BufferLineCycleNext<CR>')

------------
-- tagbar --
------------

vim.keymap.set('n', '<leader>b', ':TagbarToggle<CR>')
vim.keymap.set('n', '<F8>', ':TagbarToggle<CR>')
vim.cmd [[
let g:tagbar_type_d = {
  \ 'ctagstype' : 'd',
  \ 'kinds'     : [
  \ 'c:classes:1:1',
  \ 'f:functions:1:1',
  \ 'T:template:1:1',
  \ 'g:enums:1:1',
  \ 'e:enumerators:0:0',
  \ 'u:unions:1:1',
  \ 's:structs:1:1',
  \ 'v:variables:1:0',
  \ 'i:interfaces:1:1',
  \ 'm:members',
  \ 'a:alias'
  \ ],
  \'sro': '.',
  \ 'kind2scope' : {
  \ 'c' : 'class',
  \ 'g' : 'enum',
  \ 's' : 'struct',
  \ 'u' : 'union',
  \ 'T' : 'template'
  \},
  \ 'scope2kind' : {
  \ 'enum'      : 'g',
  \ 'class'     : 'c',
  \ 'struct'    : 's',
  \ 'union'     : 'u',
  \ 'template'  : 'T'
  \ },
  \ 'ctagsbin' : 'dscanner',
  \ 'ctagsargs' : ['--ctags']
  \ }
]]


------------------
-- smart splits --
------------------
opts = {
  ignored_filetypes = { "nofile", "quickfix", "qf", "prompt" },
  ignored_buftypes = { "nofile" },
}

--------------
-- whichkey --
--------------

local wk = require('which-key')
wk.add({
  { "<leader>f", group = "Find (Telescope)", icon = '' },
  { "<leader>fb", desc = "Find Buffer", icon = ''  },
  { "<leader>ff", desc = "Find File", icon = ''  },
  { "<leader>fh", desc = "Help Tags", icon = ''  },
  { "<leader>fs", desc = "LSP Document Symbols", icon = ''  },
  { "<leader>fw", desc = "Find Text", icon = ''  },


  { "<leader>l", group = "LSP", icon = ''  },
  { "<leader>lD", desc = "Diagnostic (floating)", icon = ''  },
  { "<leader>lc", desc = "Code Action", icon = ''  },
  { "<leader>ld", desc = "Diagnostic (setlocalist)", icon = ''  },
  { "<leader>lr", desc = "Rename", icon = ''  },

  { "<leader>lw", group = "Actions With Workspaces", icon = ''  },
  { "<leader>lwa", desc = "Add Workspace", icon = ''  },
  { "<leader>lwl", desc = "Show Workspace Directories", icon = ''  },
  { "<leader>lwr", desc = "Remove Workspace", icon = ''  },

  { "<leader>b", desc = "Tag Bar", icon = '' },
  { "<leader>c", desc = "Close Buffer", icon = '' },
  { "<leader>e", desc = "File Manager", icon = '' },
  { "<leader>q", desc = "Close All + Quit", icon = ''  },
  { "<leader>t", desc = "Horizontal Terminal", icon = ''  },
  { "<leader>v", desc = "File Manager In Window", icon = ''  },
  { "<leader>w", desc = "Save", icon = ''  },
})

