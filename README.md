# Apocalvim

This project is a single-file configuration for Neovim.  
Inspired by the [LunarVim](https://www.lunarvim.org/),
[AstroNvim](https://astronvim.com/) and other similar projects.

## Used plugins:

* [Lazy](https://github.com/folke/lazy.nvim);
* [Neo-tree](https://github.com/nvim-neo-tree/neo-tree.nvim);
* [Bufferline](https://github.com/akinsho/bufferline.nvim);
* [Telescope](https://github.com/nvim-telescope/telescope.nvim);
* [ToggleTerm](https://github.com/akinsho/toggleterm.nvim);
* [Which-Key](https://github.com/folke/which-key.nvim);
* LSP-oriented plugins;
* etc.

## Some important shortcuts

| Key | Action |
|-|-|
| Space+e | Toggle side file navigator |
| Space+v | File navigator as window |
| Space+c | Close the current buffer |
| Space+t | Terminal |
| Shift+h | Switch to buffer on the left |
| Shift+l | Swith to buffer on the right |
| Ctrl+h | Switch to window on the left |
| Ctrl+l | Switch to window on the right |

## System dependencies

### Packages for Debian-based distributions

```bash
apt install ripgrep fd-find xclip clipit
```

NodeJS (doubtful, but official installation method):

```bash
curl -fsSL https://deb.nodesource.com/setup_22.x | bash - &&\
apt-get install -y nodejs npm
```

Better install the latest version of
[Neovim](https://github.com/neovim/neovim/blob/master/INSTALL.md).

### Packages for Arch-based distributions

```bash
yay -S neovim ripgrep fd xclip clipit nodejs npm
```

### Nerd fonts

Some plugins use characters from [Nerd fonts](https://www.nerdfonts.com/).  
Example with "DejaVu Sans Mono" (you should have `unzip` and `wget`):

```bash
FONT_NAME=DejaVuSansMono
FONT_VERSION=3.3.0
mkdir -p temp
cd temp
BASE_URL=https://github.com/ryanoasis/nerd-fonts/releases/download/
wget ${BASE_URL}/v${FONT_VERSION}/${FONT_NAME}.zip
unzip *.zip
mkdir -p ~/.local/share/fonts
mv *.ttf ~/.local/share/fonts/
fc-cache -vf
cd ..
rm -r temp
```

Other good fonts:

- UbuntuMono
- Terminus
- InconsolataLGC
- Go-Mono
- Hack
- JetBrainsMono
